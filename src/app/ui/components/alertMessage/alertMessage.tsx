import React, {Component} from 'react';
import {connect} from 'react-redux';
import i18next from 'i18next';

interface IAlertMessageProps {
    language: string;
}

class AlertMessage extends Component<IAlertMessageProps, {}> {

    render() {
        return <p>{i18next.t('AMOUNT_OF_MESSAGES', {count: 21})}</p>;

    }
}

const mapStateToProps = ({translates: {language}}: any): IAlertMessageProps => {
    return {
        language
    };
};

export default connect(mapStateToProps)(AlertMessage);
