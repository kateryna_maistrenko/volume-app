import React, {Component} from 'react';
import i18n from 'i18next';
import {connect} from 'react-redux';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import {changeLanguageCreator as changeLanguage} from '../../../core/modules/translates/creators/creators';

import './changeLanguage.scss';
import {setLanguage} from '../../../core/modules/translates/utils/i18n';


interface IChangeLangProps {
    language: string
}

interface IDispatchProps {
    changeLanguage: any
}

type Props = IChangeLangProps & IDispatchProps;

class ChangeLanguage extends Component<Props> {

    render() {
        return (
            <div>
                <ButtonToolbar className='btn-toolbar'>
                    <Button variant='info' className='btn-toolbar__btn'
                            onClick={() => this.props.changeLanguage('en')}>
                        en
                    </Button>
                    <Button variant='info' className='btn-toolbar__btn'
                            onClick={() => this.props.changeLanguage('ru')}>
                        ru
                    </Button>
                    <Button variant='info' className='btn-toolbar__btn'
                            onClick={() => this.props.changeLanguage('uk')}>
                        ua
                    </Button>
                </ButtonToolbar>
                <p>{i18n.t('AMOUNT_OF_MESSAGES', {count: 21})}</p>
            </div>
        );
    }
}

const mapStateToProps = ({translates: {language}}: any): IChangeLangProps => {
    setLanguage(language);
    return {
        language
    };
};

const mapDispatchToProps: IDispatchProps = {
    changeLanguage: a => changeLanguage(a),
};

export default connect<IChangeLangProps, IDispatchProps, any>(mapStateToProps, mapDispatchToProps)(ChangeLanguage);
