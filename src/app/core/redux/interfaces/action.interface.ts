import {Action} from 'redux';

export interface IActionWithPayload<T extends string, P> extends Action<T>{
    payload: P;
}
