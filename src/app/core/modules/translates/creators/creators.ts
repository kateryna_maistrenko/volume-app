import * as actions from '../actions/changeLangActions';
import {LANGUAGE} from '../reducers/reducer';

export const changeLanguageCreator = (lang) => dispatch => {
    dispatch(actions.changeLanguage(lang));
    window.localStorage.setItem(LANGUAGE, lang);
};
