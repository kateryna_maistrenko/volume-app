import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setLanguage} from '../../utils/i18n';

interface ITranslatesProps {
    language: string
}

class TranslatesContainer extends Component<ITranslatesProps> {
    render() {
        return <div>{this.props.children}</div>;
    }
}

const mapStateToProps = ({translates: {language}}: any): ITranslatesProps => {
    setLanguage(language);
    return {
        language
    };
};

export default connect<ITranslatesProps>(mapStateToProps)(TranslatesContainer);
