import i18n from 'i18next';

export const setLanguage = (language: string): void => {
    i18n.init({
        lng: language,
        resources: {[language]: require(`../static/${language}.json`)}
    });
};
