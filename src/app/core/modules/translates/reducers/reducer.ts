import {Action} from 'redux';
import {IActionWithPayload} from '../../../redux/interfaces/action.interface';
import {IStore} from '../interfaces/languageStore.interface';

export enum LANGUAGE_ACTIONS {
    CHANGE = 'CHANGE_LANGUAGE'
}

export const LANGUAGE = 'language';
export const DEFAULT_LANGUAGE = 'en';

const initialState = {
    language: window.localStorage.getItem(LANGUAGE) || DEFAULT_LANGUAGE
};

export const reducer = (state: IStore = initialState, action: Action) => {
    switch (action.type) {
        case LANGUAGE_ACTIONS.CHANGE:
            return changeLang(state, action as IActionWithPayload<LANGUAGE_ACTIONS, string>);
        default:
            return state;
    }
};

export function changeLang(state: IStore, action: IActionWithPayload<LANGUAGE_ACTIONS, string>) {
    return {
        ...state,
        language: action.payload
    };
}
