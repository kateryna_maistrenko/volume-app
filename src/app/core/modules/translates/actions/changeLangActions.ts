import {LANGUAGE_ACTIONS} from '../reducers/reducer';
import {IActionWithPayload} from '../../../redux/interfaces/action.interface';

export const changeLanguage = (lang: string): IActionWithPayload<LANGUAGE_ACTIONS, string> => ({
    type: LANGUAGE_ACTIONS.CHANGE,
    payload: lang
});
