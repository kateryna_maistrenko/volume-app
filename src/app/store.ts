import {applyMiddleware, combineReducers, compose, createStore, ReducersMapObject} from 'redux';
import thunk from 'redux-thunk';
import {reducer as translates} from './core/modules/translates/reducers/reducer';
import {IStore} from './core/modules/translates/interfaces/languageStore.interface';

/**
 * Added for middle wares
 */
const middlewares = [
    thunk
];

// @ts-ignore
const composeEnhancers =
    typeof window === 'object' &&
    window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] ?
        window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__']({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) : compose;

const enhancer = composeEnhancers(
    applyMiddleware(...middlewares),
);

/**
 * Added for multiply reducers
 */
const appReducers: ReducersMapObject = {
    translates,
};
export const reducers = combineReducers(appReducers);

const store: IStore = createStore(reducers, enhancer);

export default store;
