import React from 'react';
import {BrowserRouter} from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import {Provider} from 'react-redux';
import store from './store';
import ChangeLanguage from './ui/components/changeLanguage/changeLanguage';
import TranslatesContainer from './core/modules/translates/components/translatesContainer/translatesContainer';
import AlertMessage from './ui/components/alertMessage/alertMessage';

import './App.css';

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <TranslatesContainer>
                <BrowserRouter>
                    <div className='App'>
                        <header className='App-header'>
                            <ChangeLanguage/>
                            <p>Volume app</p>
                            <ButtonToolbar className='btnToolbar'>
                                <Button className='button' variant='outline-primary' size='lg'>Primary</Button>
                                <Button variant='info'>Info</Button>
                            </ButtonToolbar>

                        </header>
                        <AlertMessage/>
                    </div>
                </BrowserRouter>
            </TranslatesContainer>
        </Provider>
    );
};

export default App;
